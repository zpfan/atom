## Get the project directory
export PROJECT_TEST=`pwd`
export PROJECT_SRC=`readlink -f project`
export PROJECT_GEN=$PROJECT_SRC
export PROJECT_GEN_OVERRIDE=$PROJECT_TEST


alias cdt='cd $PROJECT_TEST;                       pwd; ls -F --color' # cd test directory where you want to run simulation
alias cds='cd $PROJECT_SRC;                        pwd; ls -F --color' # cd src
alias cdv='cd $PROJECT_SRC/verify;                  pwd; ls -F --color' # cd verif
alias cdus='cd $PROJECT_SRC/verify/svtb/uvm_sim; pwd; ls -F --color' # cd uvm_sim
alias cdd='cd $PROJECT_SRC/design;                 pwd; ls -F --color' # cd design
alias spt='cd $PROJECT_SRC/script;                pwd; ls -F --color' # scripts
alias run='./run.sh'

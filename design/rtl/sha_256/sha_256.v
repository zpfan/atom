/*================================================================================
// File Name: sha_256.v
// Author: atom
// Created Time: 2023年11月14日 星期二 20时37分12秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
module sha_256(
    input               clk,
    input               rst_n,

    input               sha_req_vld,
    input               sha_req_eop,
    input   [511:0]     sha_req_data,
    input   [8:0]       sha_req_bit_cnt,
    output              sha_req_rdy,

    output              sha_rsp_vld,
    output  [255:0]     sha_rsp_data
);

sha_256_padding x_sha256_padding(
    .clk(clk),
    .rst_n(rst_n),

    .msg_eop(sha_req_eop),
    .msg_vld(sha_req_vld),
    .msg_dat(sha_req_data[511:0]),
    .msg_bit_cnt(sha_req_bit_cnt[8:0]), //0 base
    .msg_rdy(sha_req_rdy),

    .dout_vld(padding_vld),
    .dout(padding_data[511:0])
);


sha_256_extend x_sha256_extend(
    .clk(clk),
    .rst_n(rst_n),

    .din_vld(padding_vld),
    .din(padding_data[511:0]),

    .dout(extend_dout),
    .dout_vld(extend_vld)
);

sha_256_compressor x_sha256_compressor(
    .clk(clk),
    .rst_n(rst_n),

    .din(extend_dout),
    .din_vld(extend_vld),

    .hash_key(sha_rsp_data[255:0]),
    .hash_key_vld(sha_rsp_vld)
);


endmodule


/*================================================================================
// File Name: sha_256_func.v
// Author: atom
// Created Time: 2023年11月12日 星期日 22时31分04秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
function [31:0] func_sha256_potr;
    input   [31:0]  din;
    input   [5:0]   shifter_num;

    begin
        assign func_sha256_potr[31:0] = (din[31:0] >> shifter_num[5:0]) | 
                                        (din[31:0] << (7'h40-shifter_num[5:0]) );
    end
endfunction


function [31:0] func_sha256_shr;
    input   [31:0]  din;
    input   [5:0]   shifter_num;

    begin
        assign func_sha256_shr[31:0] = din[31:0] >> shifter_num[5:0];
    end 
endfunction


function [31:0] func_sha256_f0;
    input   [31:0]  din;

    begin
        assign func_sha256_f0[31:0] = func_sha256_potr(din[31:0],6'd7) + 
                                      func_sha256_potr(din[31:0],6'd18) +
                                      func_sha256_shr(din[31:0],6'd3);
    end
endfunction


function [31:0] func_sha256_f1;
    input   [31:0]  din;

    begin
        assign func_sha256_f1[31:0] = func_sha256_potr(din[31:0],6'd17) + 
                                      func_sha256_potr(din[31:0],6'd19) +
                                      func_sha256_shr(din[31:0],6'd10);
    end 
endfunction


function [31:0] func_sha256_f2;
    input   [31:0]  din;

    begin
        assign func_sha256_f2[31:0] = func_sha256_potr(din[31:0],6'd2) + 
                                      func_sha256_potr(din[31:0],6'd13) +
                                      func_sha256_potr(din[31:0],6'd22);
    end 
endfunction


function [31:0] func_sha256_f3;
    input   [31:0]  din;

    begin
        assign func_sha256_f3[31:0] = func_sha256_potr(din[31:0],6'd6) + 
                                      func_sha256_potr(din[31:0],6'd11) +
                                      func_sha256_potr(din[31:0],6'd25);
    end 
endfunction


function [31:0] func_sha256_maj;
    input   [31:0]  a;
    input   [31:0]  b;
    input   [31:0]  c;

    begin
        assign func_sha256_maj[31:0] = (a[31:0] & b[31:0]) ^ (a[31:0] & c[31:0]) ^ (b[31:0] & c[31:0]);
    end 
endfunction


function [31:0] func_sha256_ch;
    input   [31:0]  e;
    input   [31:0]  f;

    begin
        assign func_sha256_ch[31:0] = (e[31:0] & f[31:0]) ^ (~e[31:0] & ~f[31:0]);
    end 
endfunction

/*================================================================================
// File Name: sha_256_compressor_group.v
// Author: atom
// Created Time: 2023年11月16日 星期四 22时43分28秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
`include "sha_256_param.vh"

module sha_256_compressor_group(
    input                           clk,
    input                           rst_n,

    input   [31:0]                  a,
    input   [31:0]                  b,
    input   [31:0]                  c,
    input   [31:0]                  d,
    input   [31:0]                  e,
    input   [31:0]                  f,
    input   [31:0]                  g,
    input   [31:0]                  h,
    
    input   [GROUP_NUM-1:0] [31:0]  k,
    input   [GROUP_NUM-1:0] [31:0]  w,

    output  [31:0]                  new_a,
    output  [31:0]                  new_b,
    output  [31:0]                  new_c,
    output  [31:0]                  new_d,
    output  [31:0]                  new_e,
    output  [31:0]                  new_f,
    output  [31:0]                  new_g,
    output  [31:0]                  new_h
);

wire    [GROUP_NUM-1:0] [31:0] a_tmp,b_tmp,c_tmp,d_tmp,e_tmp,f_tmp,g_tmp,h_tmp;

assign a_tmp[0] = a[31:0];
assign b_tmp[0] = b[31:0];
assign c_tmp[0] = c[31:0];
assign d_tmp[0] = d[31:0];
assign e_tmp[0] = e[31:0];
assign f_tmp[0] = f[31:0];
assign g_tmp[0] = g[31:0];
assign h_tmp[0] = h[31:0];


generate
genvar i;
for(i=0;i<GROUP_NUM;i=i+1)begin:loop_com_group
        sha_256_compressor_sub x_com_sub_i(
            .a(a_tmp[i]),
            .b(b_tmp[i]),
            .c(c_tmp[i]),
            .d(d_tmp[i]),
            .e(e_tmp[i]),
            .f(f_tmp[i]),
            .g(g_tmp[i]),
            .h(h_tmp[i]),
            
            .k(k[i]),
            .w(w[i]),
        
            .new_a(a_tmp[i+1]),
            .new_b(b_tmp[i+1]),
            .new_c(c_tmp[i+1]),
            .new_d(d_tmp[i+1]),
            .new_e(e_tmp[i+1]),
            .new_f(f_tmp[i+1]),
            .new_g(g_tmp[i+1]),
            .new_h(h_tmp[i+1])
        );
end
endgenerate


always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        new_a[31:0] <= 32'b0;
        new_b[31:0] <= 32'b0;
        new_c[31:0] <= 32'b0;
        new_d[31:0] <= 32'b0;
        new_e[31:0] <= 32'b0;
        new_f[31:0] <= 32'b0;
        new_g[31:0] <= 32'b0;
        new_h[31:0] <= 32'b0;
    end
    else begin
        new_a[31:0] <= a_tmp[GROUP_NUM-1];
        new_b[31:0] <= b_tmp[GROUP_NUM-1];
        new_c[31:0] <= c_tmp[GROUP_NUM-1];
        new_d[31:0] <= d_tmp[GROUP_NUM-1];
        new_e[31:0] <= e_tmp[GROUP_NUM-1];
        new_f[31:0] <= f_tmp[GROUP_NUM-1];
        new_g[31:0] <= g_tmp[GROUP_NUM-1];
        new_h[31:0] <= h_tmp[GROUP_NUM-1];
    end
end 



endmodule

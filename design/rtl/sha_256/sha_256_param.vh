parameter   A_INIT   =   32'h6a09e667;
parameter   B_INIT   =   32'hbb67ae85;
parameter   C_INIT   =   32'h3c6ef372;
parameter   D_INIT   =   32'ha54ff53a;
parameter   E_INIT   =   32'h510e527f;
parameter   F_INIT   =   32'h9b05688c;
parameter   G_INIT   =   32'h1f83d9ab;
parameter   H_INIT   =   32'h5be0cd19;

parameter   PIPELINE_NUM    =   8;
parameter   GROUP_NUM       =   64/PIPELINE_NUM;

/*================================================================================
// File Name: sha_256_compressor_sub.v
// Author: atom
// Created Time: 2023年11月16日 星期四 21时01分21秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
module sha_256_compressor_sub(
    input   [31:0]  a,
    input   [31:0]  b,
    input   [31:0]  c,
    input   [31:0]  d,
    input   [31:0]  e,
    input   [31:0]  f,
    input   [31:0]  g,
    input   [31:0]  h,
    
    input   [31:0]  k,
    input   [31:0]  w,

    output  [31:0]  new_a,
    output  [31:0]  new_b,
    output  [31:0]  new_c,
    output  [31:0]  new_d,
    output  [31:0]  new_e,
    output  [31:0]  new_f,
    output  [31:0]  new_g,
    output  [31:0]  new_h
);

wire [31:0]     t1,t2;

assign t1[31:0] = h[31:0] + func_sha256_f3(e[31:0]) + func_sha256_ch(e[31:0],f[31:0]) +
                  k[31:0] + w[31:0];

assign t2[31:0] = func_sha256_f2(a[31:0]) + func_sha256_maj(a[31:0],b[31:0],c[31:0]);

assign new_a[31:0] = t1[31:0] + t2[31:0];
assign new_b[31:0] = a[31:0];
assign new_c[31:0] = b[31:0];
assign new_d[31:0] = c[31:0];
assign new_e[31:0] = d[31:0] + t1[31:0];
assign new_f[31:0] = e[31:0];
assign new_g[31:0] = f[31:0];
assign new_h[31:0] = g[31:0];

endmodule

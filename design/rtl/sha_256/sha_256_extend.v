/*================================================================================
// File Name: sha_256_extend.v
// Author: atom
// Created Time: 2023年11月15日 星期三 23时30分13秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
module sha_256_extend(
    input                       clk,
    input                       rst_n,

    input                       din_vld,
    input       [511:0]         din,

    output  reg [63:0] [31:0]   dout,
    output  reg                 dout_vld
);


wire    [63:0] [31:0]       data;

generate
genvar j;
    for(j=0;i<16;j=j+1)begin:loop_dispatch0
        assign data[j] = din[(j+1)*32-1-:32];
        assign data[j+16] = func_sha256_f1(data[j+16-2]) + data[j+16-7] +
                            func_sha256_f0(data[j+16-15])+ data[j+16-16];

        assign data[j+32] = func_sha256_f1(data[j+32-2]) + data[j+32-7] +
                            func_sha256_f0(data[j+32-15])+ data[j+32-16];

        assign data[j+48] = func_sha256_f1(data[j+48-2]) + data[j+48-7] +
                            func_sha256_f0(data[j+48-15])+ data[j+48-16];
    end
endgenerate



always @(posedge clk or negedge rst_n)begin
    if(!rst_n)
        dout_vld <= 1'b0;
    else
        dout_vld <= din_vld;
end

integer i;

always @(posedge clk or negedge rst_n)begin
    if(!rst_n)
        for(i=0;i<64;i=i+1)
            dout[i] <= 32'b0;
    else
        for(i=0;i<64;i=i+1)
            dout[i] <= data[i];
end

endmodule

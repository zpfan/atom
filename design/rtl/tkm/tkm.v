/*================================================================================
// File Name: tkm.v
// Author: atom
// Created Time: 2023年12月10日 星期日 20时30分55秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
module tkm(

    input                   clk,
    input                   rst_n,

    input                   pm_tkm_req_vld,
    input   [3:0]           pm_tkm_req_port,
    input   [10:0]          pm_tkm_req_bcnt,

    output                  tkm_pm_rsp_vld,
    output  [3:0]           tkm_pm_rsp_data,
    
    input                   csr_tkm_mode,
    input   [3:0][11:0]     csr_tkm_cir,    // unit is 1 Mbps so 
    input   [3:0][11:0]     csr_tkm_pir,
    input   [3:0][19:0]     csr_tkm_cbs,
    input   [3:0][19:0]     csr_tkm_pbs,
    output  [3:0][31:0]     csr_tkm_cbc_sta,
    output  [3:0][31:0]     csr_tkm_pbc_sta

);


wire    [3:0]       rsp_vld;

assign tkm_pm_rsp_vld = |rsp_vld[3:0];


genvar i;
generate
    for(i=0;i<4;i=i+1)begin:loop_tku
        tku x_tku(
            .clk(clk),
            .rst_n(rst_n),

            .req_vld(pm_tkm_req_vld & pm_tkm_req_port[i]),
            .req_bcnt(pm_tkm_req_bcnt[10:0]),
            .rsp_vld(rsp_vld[i]),
            .rsp_ack(tkm_pm_rsp_data[i]),        //0:ack   1:nack

            .csr_tkm_mode(csr_tkm_mode),
            .csr_tkm_cir(csr_tkm_cir[i]),    // unit is 1 Mbps so 
            .csr_tkm_pir(csr_tkm_pir[i]),
            .csr_tkm_cbs(csr_tkm_cbs[i]),
            .csr_tkm_pbs(csr_tkm_pbs[i]),
            .csr_tkm_cbc_sta(csr_tkm_cbc_sta[i]),
            .csr_tkm_pbc_sta(csr_tkm_pbc_sta[i])
);
    end
endgenerate


endmodule

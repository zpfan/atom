parameter   COL_MIXER_FACTOR__0   = 16'h3112;
parameter   COL_MIXER_FACTOR__1   = 16'h1123;
parameter   COL_MIXER_FACTOR__2   = 16'h1231;
parameter   COL_MIXER_FACTOR__3   = 16'h2311;
parameter   COL_DEMIXER_FACTOR__0 = 16'hbd9e;  
parameter   COL_DEMIXER_FACTOR__1 = 16'hd9eb;  
parameter   COL_DEMIXER_FACTOR__2 = 16'h9ebd;  
parameter   COL_DEMIXER_FACTOR__3 = 16'hebd9;  


parameter   KEY_EXTEND_FACTOR__0  = 8'h01;
parameter   KEY_EXTEND_FACTOR__1  = 8'h02;
parameter   KEY_EXTEND_FACTOR__2  = 8'h04;
parameter   KEY_EXTEND_FACTOR__3  = 8'h08;
parameter   KEY_EXTEND_FACTOR__4  = 8'h10;
parameter   KEY_EXTEND_FACTOR__5  = 8'h20;
parameter   KEY_EXTEND_FACTOR__6  = 8'h40;
parameter   KEY_EXTEND_FACTOR__7  = 8'h80;
parameter   KEY_EXTEND_FACTOR__8  = 8'h1b;
parameter   KEY_EXTEND_FACTOR__9  = 8'h36;

parameter   KEY_MODE_128          =  2'b00;
parameter   KEY_MODE_192          =  2'b01;
parameter   KEY_MODE_256          =  2'b10;

parameter   KEY_128_ROUND_NUM     =  4'd10;
parameter   KEY_192_ROUND_NUM     =  4'd12;
parameter   KEY_256_ROUND_NUM     =  4'd14;


/*----------------------------------------------------------------
 |  S0  |         |  S1  |   
 |  S1  |         |  S2  |  
 |  S2  |    ->   |  S3  |  
 |  S3  |         |  S0  |  
----------------------------------------------------------------*/

module aes_key_shifter_sbox(
        input       [3:0][7:0]      key_col,
        input       [15:0][255:0]   sbox,

        output      [3:0][7:0]      key_col_s
);


wire    [3:0][7:0]  key_col_map;

generate
genvar i;
for(i=0;i<4;i++)begin:loop_key_col_transfor
aes_one_line_shifter_sbox
    x_aes_one_line_shifter_sbox_i(
    .sbox(sbox),
    .data(key_col[i]),

    .data_s(key_col_map[i])
);
end
endgenerate


assign key_col_s[0] = key_col_map[1];
assign key_col_s[1] = key_col_map[2];
assign key_col_s[2] = key_col_map[3];
assign key_col_s[3] = key_col_map[0];



endmodule

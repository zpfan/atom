module aes_line_shifter_sbox(
    input   [15:0][255:0]   sbox,
    input   [15:0][7:0]     matrix,

    output  [15:0][7:0]     matrix_s
);

/*----------------------------------------------------------------
 |  S0  |  S4  | S8  |  S12  |       |  S0  |  S4  | S8  |  S12  | 
 |  S1  |  S5  | S9  |  S13  |       |  S5  |  S9  | S13 |  S1   |
 |  S2  |  S6  | S10 |  S14  |  ->   |  S10 |  S14 | S2  |  S6   |
 |  S3  |  S7  | S11 |  S15  |       |  S15 |  S3  | S7  |  S11  |
----------------------------------------------------------------*/

wire    [15:0][7:0]     matrix_map;

generate
genvar i;
for(i=0;i<16;i++)begin:loop_matrix_transfor
aes_one_line_shifter_sbox
    x_aes_one_line_shifter_sbox_i(
    .sbox(sbox),
    .data(matrix[i]),

    .data_s(matrix_map[i])
);
end
endgenerate

assign matrix_s[0]  = matrix_map[0];
assign matrix_s[1]  = matrix_map[5];
assign matrix_s[2]  = matrix_map[10];
assign matrix_s[3]  = matrix_map[15];
assign matrix_s[4]  = matrix_map[4];
assign matrix_s[5]  = matrix_map[9];
assign matrix_s[6]  = matrix_map[14];
assign matrix_s[7]  = matrix_map[3];
assign matrix_s[8]  = matrix_map[8];
assign matrix_s[9]  = matrix_map[13];
assign matrix_s[10] = matrix_map[2];
assign matrix_s[11] = matrix_map[7];
assign matrix_s[12] = matrix_map[12];
assign matrix_s[13] = matrix_map[1];
assign matrix_s[14] = matrix_map[6];
assign matrix_s[15] = matrix_map[11];


endmodule

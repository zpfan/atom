module aes_key_add(
    
    input                   csr_aes_work_mode,

    input       [127:0]     key,
    input       [127:0]     din,
    input       [127:0]     vector,

    output      [15:0][7:0] dout
);

wire    [127:0]     pre_data;
wire    [127:0]     data_c;

assign pre_data[127:0] = din[127:0] ^ vector[127:0];

assign data_c[127:0] = csr_aes_work_mode ? pre_data[127:0] ^ key[127:0] :
                                           din[127:0] ^ key[127:0]; 


generate
genvar i;
    for(i=0;i<16;i++)begin:loop_data
        assign dout[i] = data_c[8*i-1-:8];
    end
endgenerate

endmodule

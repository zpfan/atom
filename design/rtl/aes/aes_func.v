function [7:0] func_matrix_one_compute;
    input   [31:0]  matrix_row;
    input   [15:0]  factor;

    begin
        func_matrix_one_compute[7:0] = matrix_row[7:0]  * factor[3:0] +
                                       matrix_row[15:8] * factor[7:4] +
                                       matrix_row[23:16]* factor[11:8]+
                                       matrix_row[31:24]* factor[15:12];
    end
endfunction

module aes_one_line_shifter_sbox(
    input   [15:0][255:0]   sbox,
    input   [7:0]           data,  //X:[7:4] Y:[3:0]

    output  [7:0]           data_s
);

wire    [255:0]     sbox_row;

encode_mux_2d #(
    .WIDTH(256),
    .CNT(16),
    .SEL_WIDTH(4)
    )x_encode_mux_sel_row(
        .din(sbox),
        .sel(data[7:4]),
        .dout(sbox_row[255:0])
        );

encode_mux #(
    .WIDTH(8),
    .CNT(16)
    )x_encode_mux_sel_data(
        .din(sbox_row[255:0]),
        .sel(data[3:0]),
        .dout(data_s[7:0])
        );

endmodule

/*================================================================================
// File Name: sm3_func.v
// Author: atom
// Created Time: 2023年11月24日 星期五 20时56分14秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
function [31:0] func_sm3_potr;
    input   [31:0]  din;
    input   [5:0]   shifter_num;

    begin
        assign func_sm3_potr[31:0] = (din[31:0] << shifter_num[5:0]) | 
                                     (din[31:0] >> (7'h40-shifter_num[5:0]) );
    end
endfunction




function [31:0] func_sm3_f0;
    input   [31:0]  din;

    begin
        assign func_sm3_f0[31:0] = din[31:0] + 
                                   func_sm3_potr(din[31:0],6'd9) +
                                   func_sm3_potr(din[31:0],6'd17);
    end
endfunction


function [31:0] func_sm3_f1;
    input   [31:0]  din;

    begin
        assign func_sm3_f1[31:0] = din[31:0] + 
                                   func_sm3_potr(din[31:0],6'd15) +
                                   func_sm3_potr(din[31:0],6'd23);
    end 
endfunction



function [31:0] func_sm3_ffj;
    input   [31:0]  x;
    input   [31:0]  y;
    input   [31:0]  z;
    input   [5:0]   index;

    begin
        assign func_sm3_ffj[31:0] = index[5:0] < 6'd15 ? 
                                    x[31:0] ^ y[31:0] ^ z[31:0] :
                                   (x[31:0] & y[31:0]) | (y[31:0] & z[31:0]) | (x[31:0] & z[31:0]);
    end 
endfunction


function [31:0] func_sm3_ggj;
    input   [31:0]  x;
    input   [31:0]  y;
    input   [31:0]  z;
    input   [5:0]   index;

    begin
        assign func_sm3_ggj[31:0] = index[5:0] < 6'd15 ?
                                    x[31:0] ^ y[31:0] ^ z[31:0] :
                                   (x[31:0] & y[31:0]) | (~x[31:0] & z[31:0]);
    end 
endfunction



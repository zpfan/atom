/*================================================================================
// File Name: sm3_compressor_group.v
// Author: atom
// Created Time: 2023年11月24日 星期五 22时50分52秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
`include "sm3.vh"

module sm3_compressor_group(
    input                           clk,
    input                           rst_n,

    input   [31:0]                  a,
    input   [31:0]                  b,
    input   [31:0]                  c,
    input   [31:0]                  d,
    input   [31:0]                  e,
    input   [31:0]                  f,
    input   [31:0]                  g,
    input   [31:0]                  h,
    
    input   [GROUP_NUM-1:0] [31:0]  w,
    input   [GROUP_NUM-1:0] [31:0]  w1,
    input   [3:0]                   pipeline_idx,    

    output  [31:0]                  new_a,
    output  [31:0]                  new_b,
    output  [31:0]                  new_c,
    output  [31:0]                  new_d,
    output  [31:0]                  new_e,
    output  [31:0]                  new_f,
    output  [31:0]                  new_g,
    output  [31:0]                  new_h
);

wire    [GROUP_NUM-1:0] [31:0] a_tmp,b_tmp,c_tmp,d_tmp,e_tmp,f_tmp,g_tmp,h_tmp;
wire    [6:0]   index_base;
wire    [31:0]  tj_sub;

assign a_tmp[0] = a[31:0];
assign b_tmp[0] = b[31:0];
assign c_tmp[0] = c[31:0];
assign d_tmp[0] = d[31:0];
assign e_tmp[0] = e[31:0];
assign f_tmp[0] = f[31:0];
assign g_tmp[0] = g[31:0];
assign h_tmp[0] = h[31:0];


assign index_base[6:0] = pipeline_idx[3:0] * GROUP_NUM;
assign tj_sub[31:0] = ((pipeline_idx[3:0]+1'b1) * GROUP_NUM < 16) ? 32'h79cc4519 : 32'h7a879d8a;

generate
genvar i;
for(i=0;i<GROUP_NUM;i=i+1)begin:loop_com_group
        sm3_compressor_sub x_com_sub_i(
            .a(a_tmp[i]),
            .b(b_tmp[i]),
            .c(c_tmp[i]),
            .d(d_tmp[i]),
            .e(e_tmp[i]),
            .f(f_tmp[i]),
            .g(g_tmp[i]),
            .h(h_tmp[i]),
            
            .w(w[i]),
            .w1(w1[i]),
            .tj(tj_sub[31:0]),
            .index(index_base[6:0]+i),
        
            .new_a(a_tmp[i+1]),
            .new_b(b_tmp[i+1]),
            .new_c(c_tmp[i+1]),
            .new_d(d_tmp[i+1]),
            .new_e(e_tmp[i+1]),
            .new_f(f_tmp[i+1]),
            .new_g(g_tmp[i+1]),
            .new_h(h_tmp[i+1])
        );
end
endgenerate


always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        new_a[31:0] <= 32'b0;
        new_b[31:0] <= 32'b0;
        new_c[31:0] <= 32'b0;
        new_d[31:0] <= 32'b0;
        new_e[31:0] <= 32'b0;
        new_f[31:0] <= 32'b0;
        new_g[31:0] <= 32'b0;
        new_h[31:0] <= 32'b0;
    end
    else begin
        new_a[31:0] <= a_tmp[GROUP_NUM-1];
        new_b[31:0] <= b_tmp[GROUP_NUM-1];
        new_c[31:0] <= c_tmp[GROUP_NUM-1];
        new_d[31:0] <= d_tmp[GROUP_NUM-1];
        new_e[31:0] <= e_tmp[GROUP_NUM-1];
        new_f[31:0] <= f_tmp[GROUP_NUM-1];
        new_g[31:0] <= g_tmp[GROUP_NUM-1];
        new_h[31:0] <= h_tmp[GROUP_NUM-1];
    end
end 



endmodule

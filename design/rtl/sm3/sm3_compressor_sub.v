/*================================================================================
// File Name: sm3_compressor_sub.v
// Author: atom
// Created Time: 2023年11月24日 星期五 22时44分29秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
module sm3_compressor_sub(
    input   [31:0]  a,
    input   [31:0]  b,
    input   [31:0]  c,
    input   [31:0]  d,
    input   [31:0]  e,
    input   [31:0]  f,
    input   [31:0]  g,
    input   [31:0]  h,
    
    input   [31:0]  w,
    input   [31:0]  w1,
    input   [31:0]  tj,
    input   [6:0]   index,

    output  [31:0]  new_a,
    output  [31:0]  new_b,
    output  [31:0]  new_c,
    output  [31:0]  new_d,
    output  [31:0]  new_e,
    output  [31:0]  new_f,
    output  [31:0]  new_g,
    output  [31:0]  new_h
);

wire [31:0]     s1,s2;
wire [31:0]     s1_tmp;
wire [31:0]     t1,t2;

assign s1_tmp[31:0] = func_sm3_potr(a[31:0],6'd12) + e[31:0] + func_sm3_potr(tj[31:0],index[5:0]);
assign s1[31:0]     = func_sm3_potr(t1_tmp[31:0],6'd7);

assign s2[31:0] = s1[31:0] ^ func_sm3_potr(a[31:0],6'd12);

assign t1[31:0] = func_sm3_ffj(a[31:0],b[31:0],c[31:0]) + d[31:0] + s2[31:0] + w1[31:0];
assign t2[31:0] = func_sm3_ggj(e[31:0],f[31:0],g[31:0]) + h[31:0] + s1[31:0] + w[31:0];


assign new_a[31:0] = t1[31:0];
assign new_b[31:0] = a[31:0];
assign new_c[31:0] = func_sm3_potr(b[31:0],6'd9);
assign new_d[31:0] = c[31:0];
assign new_e[31:0] = func_sm3_f0(t2[31:0]);
assign new_f[31:0] = e[31:0];
assign new_g[31:0] = func_sm3_potr(f[31:0],6'd19);
assign new_h[31:0] = g[31:0];

endmodule

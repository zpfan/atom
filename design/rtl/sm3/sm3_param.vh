parameter   A_INIT   =   32'h7380166f;
parameter   B_INIT   =   32'h4914b2b9;
parameter   C_INIT   =   32'h172442d7;
parameter   D_INIT   =   32'hda8a0600;
parameter   E_INIT   =   32'ha96f30bc;
parameter   F_INIT   =   32'h163118aa;
parameter   G_INIT   =   32'he38dee4d;
parameter   H_INIT   =   32'hb0fb0e4e;

parameter   PIPELINE_NUM    =   8;
parameter   GROUP_NUM       =   64/PIPELINE_NUM;

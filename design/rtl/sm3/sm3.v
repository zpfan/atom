/*================================================================================
// File Name: sm3.v
// Author: atom
// Created Time: 2023年11月24日 星期五 23时05分52秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
module sm3(
    input               clk,
    input               rst_n,

    input               sm3_req_vld,
    input               sm3_req_eop,
    input   [511:0]     sm3_req_data,
    input   [8:0]       sm3_req_bit_cnt,
    output              sm3_req_rdy,

    output              sm3_rsp_vld,
    output  [255:0]     sm3_rsp_data
);

sm3_padding x_sm3_padding(
    .clk(clk),
    .rst_n(rst_n),

    .msg_eop(sha_req_eop),
    .msg_vld(sha_req_vld),
    .msg_dat(sha_req_data[511:0]),
    .msg_bit_cnt(sha_req_bit_cnt[8:0]), //0 base
    .msg_rdy(sha_req_rdy),

    .dout_vld(padding_vld),
    .dout(padding_data[511:0])
);


sm3_extend x_sm3_extend(
    .clk(clk),
    .rst_n(rst_n),

    .din_vld(padding_vld),
    .din(padding_data[511:0]),

    .dout(extend_dout),
    .dout1(extend_dout1)
    .dout_vld(extend_vld)
);

sm3_compressor x_sm3_compressor(
    .clk(clk),
    .rst_n(rst_n),

    .din(extend_dout),
    .din1(extend_dout1)
    .din_vld(extend_vld),

    .hash_key(sha_rsp_data[255:0]),
    .hash_key_vld(sha_rsp_vld)
);


endmodule


`ifndef common_funcs_vh
`define common_funcs_vh

function int log2;
  input int value;
  log2 = 1;
  while(value > (2**log2))
    log2 = log2+1;
endfunction

// Add one more bit when value=2**log2,
// used for counter width calculation
function int log2_cnt;
  input int value;
  log2_cnt = 1;
  while(value >= (2**log2_cnt))
    log2_cnt = log2_cnt+1;
endfunction

`endif

/////////////////////////////////////////////////////
// File Name: rptr_empty.v
// Author: zeping fan
// mail:   zpfan007@163.com
// Created Time: 2023年05月16日 星期二 17时32分07秒
/////////////////////////////////////////////////////
//

module  rptr_empty#(
    parameter   PTR_WIDTH       =   5,
                DATA_FLOAT_OUT  =   1'b0
)
(
input                   rd_clk_i,
input                   rstn_i,
input                   rd_en_i,
input   [PTR_WIDTH:0]   wptr_gray_i,      
output                  rd_empty_o,
output  [PTR_WIDTH-1:0] rd_addr_o,
output  [PTR_WIDTH:0]   rptr_gray_o,
output  [PTR_WIDTH:0]   rptr_bin_o
);

wire    [PTR_WIDTH:0]   rptr_bin_nxt;

reg     [PTR_WIDTH:0]   rptr_bin_o;


always @(posedge rd_clk_i or negedge rstn_i)begin
    if(!rstn_i)
        rptr_bin_o[PTR_WIDTH:0] <= {{PTR_WIDTH+1}{1'b0}};
    else
        rptr_bin_o[PTR_WIDTH:0] <= rptr_bin_nxt[PTR_WIDTH:0];
end


assign  rptr_bin_nxt[PTR_WIDTH:0] = (rd_en_i & ~rd_empty_o)? rptr_bin_o[PTR_WIDTH:0]+1'b1 : rptr_bin_o[PTR_WIDTH:0];

assign  rptr_gray_o[PTR_WIDTH:0] = rptr_bin_o[PTR_WIDTH:0] ^ (rptr_bin_o[PTR_WIDTH:0]>>1);

assign  rd_addr_o[PTR_WIDTH-1:0] = (DATA_FLOAT_OUT)? rptr_bin_nxt[PTR_WIDTH-1:0] : rptr_bin_o[PTR_WIDTH-1:0];

assign  rd_empty_o = (wptr_gray_i[PTR_WIDTH:0] == rptr_gray_o[PTR_WIDTH:0]);    

endmodule

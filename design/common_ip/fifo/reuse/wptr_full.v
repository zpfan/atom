/////////////////////////////////////////////////////
// File Name: wptr_full.v
// Author: zeping fan
// mail:   zpfan007@163.com
// Created Time: 2023年05月16日 星期二 20时56分46秒
/////////////////////////////////////////////////////
//

module wptr_full#(
    parameter   PTR_WIDTH = 5
)
(
input                   wr_clk_i,
input                   rstn_i,
input                   wr_en_i,
input   [PTR_WIDTH:0]   rptr_gray_i,
output  [PTR_WIDTH:0]   wptr_gray_o,
output  [PTR_WIDTH-1:0] wr_addr_o,
output                  wr_full_o,
output  [PTR_WIDTH:0]   wptr_bin_o

);

wire    [PTR_WIDTH:0]   wptr_bin_nxt;

reg     [PTR_WIDTH:0]   wptr_bin_o;

always@(posedge wr_clk_i or negedge rstn_i)begin
    if(!rstn_i)
        wptr_bin_o[PTR_WIDTH:0] <= {{PTR_WIDTH+1}{1'b0}};
    else
        wptr_bin_o[PTR_WIDTH:0] <= wptr_bin_nxt[PTR_WIDTH:0];
end
    
assign  wptr_bin_nxt[PTR_WIDTH:0] = (wr_en_i && ~wr_full_o)? wptr_bin_o[PTR_WIDTH:0]+1'b1 : wptr_bin_o[PTR_WIDTH:0]; 

assign  wptr_gray_o[PTR_WIDTH:0] = wptr_bin_o[PTR_WIDTH:0] ^ (wptr_bin_o[PTR_WIDTH:0]>>1);

assign  wr_addr_o[PTR_WIDTH-1:0] = wptr_bin_o[PTR_WIDTH-1:0];

assign  wr_full_o = (rptr_gray_i[PTR_WIDTH:0] == {~wptr_gray_o[PTR_WIDTH-:2],wptr_gray_o[PTR_WIDTH-2:0]});


endmodule
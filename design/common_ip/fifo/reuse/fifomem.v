/////////////////////////////////////////////////////
// File Name: fifomem.v
// Author: zeping fan
// mail:   zpfan007@163.com
// Created Time: 2023年05月16日 星期二 17时01分06秒
/////////////////////////////////////////////////////


module fifomem#(    
    parameter   DATA_WIDTH  = 8,
                FIFO_DEPTH  = 16,
                DATA_FLOAT_OUT = 1'b0
)(
rstn_i,
wr_clk_i,
wr_en_i,
wr_addr_i,
wr_data_i,
rd_clk_i,
rd_en_i,
rd_addr_i,
rd_data_o
); 
parameter   ADDR_WIDTH  = $clog2(FIFO_DEPTH);

input                       rstn_i;
input                       wr_clk_i;
input                       wr_en_i;
input   [ADDR_WIDTH-1:0]    wr_addr_i;
input   [DATA_WIDTH-1:0]    wr_data_i;
input                       rd_clk_i;
input                       rd_en_i;
input   [ADDR_WIDTH-1:0]    rd_addr_i;
output  [DATA_WIDTH-1:0]    rd_data_o;


reg [DATA_WIDTH-1:0]    mem [FIFO_DEPTH-1:0];

reg [DATA_WIDTH-1:0]    rd_data_o;   

  
always @(posedge wr_clk_i or negedge rstn_i)begin
    if(!rstn_i)
        for(integer i=0;i<FIFO_DEPTH;i=i+1)
            mem[i] <= {DATA_WIDTH{1'b0}};
    else if(wr_en_i)
        mem[wr_addr_i] <= wr_data_i;
end

always @(posedge rd_clk_i or negedge rstn_i)begin
    if(!rstn_i)
        rd_data_o <= {DATA_WIDTH{1'b0}};
    else if(DATA_FLOAT_OUT)
        rd_data_o <= mem[rd_addr_i];
    else if(rd_en_i)
        rd_data_o <= mem[rd_addr_i];
end

endmodule

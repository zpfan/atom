/*================================================================================
// File Name: binary_demux_2d.v
// Author: atom
// Created Time: 2023年11月12日 星期日 21时55分13秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
`include "common_funcs.vh"

module  binary_demux_2d#(
    parameter   WIDTH   =   1,
                CNT     =   1,
                LOG2_CNT=   log2(CNT)   
)(
    input   [WIDTH-1:0]             din,
    input   [LOG2_CNT-1:0]          sel,
    output  [CNT-1:0][WIDTH-1:0]    dout
);



generate
genvar i;
    for(i = 0; i < CNT; i = i+1)begin:loop_onehot_demux
        assign dout[i] = sel[LOG2_CNT-1:0]==i ? din[WIDTH-1:0] : {(WIDTH){1'b0}};
    end 
endgenerate


endmodule

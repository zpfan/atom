/*================================================================================
// File Name: binary_mux.v
// Author: atom
// Created Time: 2023年11月12日 星期日 20时26分11秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
`include "common_funcs.vh"

module binary_mux#(
    parameter   WIDTH   =   1,
                CNT     =   1,
                LOG2_CNT=   log2(CNT)
)(
    input   [CNT*WIDTH-1:0]         din,
    output  [WIDTH-1:0]             dout,
    input   [LOG2_CNT-1:0]          sel
);

wire    [CNT*WIDTH-1:0] din_shifter;

assign din_shifter[CNT*WIDTH-1:0] = din[CNT*WIDTH-1:0] >> (sel[LOG2_CNT-1:0]*WIDTH);

assign dout[WIDTH-1:0] = din_shifter[WIDTH-1:0];

endmodule

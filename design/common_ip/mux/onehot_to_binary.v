/*================================================================================
// File Name: onehot_to_binary.v
// Author: atom
// Created Time: 2023年11月12日 星期日 16时26分15秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
`include    "common_funcs.vh"

module onehot_to_binary#(
    parameter   DIN_WIDTH  = 1,
                DOUT_WIDTH = log2(DIN_WIDTH)
)(
    input   [DIN_WIDTH-1:0]     onehot_in,
    output  [DOUT_WIDTH-1:0]    bin_out
);


wire    [DIN_WIDTH-1:0][DOUT_WIDTH-1:0]     data;

generate
genvar i;
    for(i = 0; i < DIN_WIDTH; i = i+1)begin:loop_binary
        assign data[i] = i;
    end 
endgenerate


onehot_mux_2d #(
    .WIDTH(DOUT_WIDTH), 
    .CNT(DIN_WIDTH)
) x_onehot_mux_2d (
  .din(data),
  .sel(onehot_in),
  .dout(bin_out)
);

endmodule

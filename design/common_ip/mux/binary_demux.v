/*================================================================================
// File Name: binary_demux.v
// Author: atom
// Created Time: 2023年11月12日 星期日 21时52分15秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
`include "common_funcs.vh"

module binary_demux#(
    parameter   WIDTH   =   1,
                CNT     =   1,
                LOG2_CNT=   log2(CNT)
)(
    input   [WIDTH-1:0]         din,
    output  [CNT*WIDTH-1:0]     dout,
    input   [LOG2_CNT-1:0]      sel  
);


assign dout[CNT*WIDTH-1:0] = { {(CNT*WIDTH-WIDTH){1'b0}}, din[WIDTH-1:0]} << (sel[LOG2_CNT-1:0]*WIDTH);

endmodule

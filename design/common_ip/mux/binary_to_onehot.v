/*================================================================================
// File Name: binary_to_onehot.v
// Author: atom
// Created Time: 2023年11月12日 星期日 16时26分29秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
module binary_to_onehot#(
    parameter   DIN_WIDTH = 1,
                DOUT_WIDTH= 1
)(
    input   [DIN_WIDTH-1:0]     bin_in,
    output  [DOUT_WIDTH-1:0]    onehot_out
);


generate
genvar i;
    for(i = 0; i < DOUT_WIDTH; i=i+1)begin:loop_onehot
        assign onehot_out[i] = bin_in[DIN_WIDTH-1:0]==i;
    end
endgenerate

endmodule

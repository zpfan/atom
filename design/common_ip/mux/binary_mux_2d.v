/*================================================================================
// File Name: binary_mux_2d.v
// Author: atom
// Created Time: 2023年11月12日 星期日 20时26分16秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
`include "common_funcs.vh"

module binary_mux_2d#(
    parameter   WIDTH   =   1,
                CNT     =   1,
                LOG2_CNT=   log2(CNT)
)(
    input   [CNT-1:0] [WIDTH-1:0]   din,
    output  [WIDTH-1:0]             dout,
    input   [LOG2_CNT-1:0]          sel
);




assign dout[WIDTH-1:0] = din[sel];

endmodule

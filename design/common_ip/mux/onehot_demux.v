/*================================================================================
// File Name: onehot_demux.v
// Author: atom
// Created Time: 2023年11月12日 星期日 20时32分09秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
`include "common_funcs.vh"

module onehot_demux#(
    parameter   WIDTH   =   1,
                CNT     =   1,
                LOG2_CNT=   log2(CNT)
)(
    input   [WIDTH-1:0]         din,
    output  [CNT*WIDTH-1:0]     dout,
    input   [CNT-1:0]           sel
);


generate
genvar i;
    for(i = 0; i < CNT; i = i+1)begin:loop_onehot_demux
        assign dout[WIDTH*(i+1)-1-:WIDTH] = sel[i] ? din[WIDTH-1:0] : {(WIDTH){1'b0}};
    end 
endgenerate

endmodule

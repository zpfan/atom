/*================================================================================
// File Name: onehot_mux.v
// Author: atom
// Created Time: 2023年11月12日 星期日 19时54分25秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
`include "common_funcs.vh"

module onehot_mux#(
    parameter   WIDTH   =   1,
                CNT     =   1,
                LOG2_CNT=   log2(CNT)
)(
    input   [CNT*WIDTH-1:0]     din,
    output  [WIDTH-1:0]         dout,
    input   [CNT-1:0]           sel
);


reg     [LOG2_CNT-1:0]  sel_idx;

wire    [CNT*WIDTH-1:0] din_shifter;

integer     i;

always @(*)begin
    for(i = 0; i < CNT; i = i+1)
        if(sel[i])
            sel_idx[LOG2_CNT-1:0] = i;
        else
            sel_idx[LOG2_CNT-1:0] = {(WIDTH){1'b0}};
end
assign din_shifter[CNT*WIDTH-1:0] = din[CNT*WIDTH-1:0] >> (sel_idx[LOG2_CNT-1:0]*WIDTH);

assign dout[WIDTH-1:0] = |sel_idx[LOG2_CNT-1:0] ? din_shifter[WIDTH-1:0] : {(WIDTH){1'b0}};

endmodule

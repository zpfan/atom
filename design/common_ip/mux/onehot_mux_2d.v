/*================================================================================
// File Name: onehot_mux_2d.v
// Author: atom
// Created Time: 2023年11月12日 星期日 20时22分18秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
`include "common_funcs.vh"

module onehot_mux_2d#(
    parameter   WIDTH   =   1,
                CNT     =   1,
                LOG2_CNT=   log2(CNT)
)(
    input   [CNT-1:0] [WIDTH-1:0]   din,
    output  [WIDTH-1:0]             dout,
    input   [CNT-1:0]               sel
);



reg     [LOG2_CNT-1:0]  sel_idx;

integer     i;

always @(*)begin
    for(i = 0; i < CNT; i = i+1)
        if(sel[i])
            sel_idx[LOG2_CNT-1:0] = i;
        else
            sel_idx[LOG2_CNT-1:0] = {(WIDTH){1'b0}};
end

assign dout[WIDTH-1:0] = |sel_idx[LOG2_CNT-1:0] ? din[sel_idx] : {(WIDTH){1'b0}};

endmodule

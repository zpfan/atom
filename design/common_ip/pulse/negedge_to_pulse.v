/*================================================================================
// File Name: negedge_to_pulse.v
// Author: atom
// Created Time: 2023年11月12日 星期日 15时52分57秒
//
//       *         *********       ****            *         *     
//      * *            *          *    *          * *       * *    
//     *   *           *         *      *        *   *     *   *   
//    *******          *         *      *       *     *   *     *  
//   *       *         *          *    *       *       * *       * 
//  *         *        *           ****       *         *         *
================================================================================*/
module negedge_to_pulse#(
    parameter   WIDTH   =   1    
)(
    input               clk,
    input               rst_n,

    input   [WIDTH-1:0] din,
    output  [WIDTH-1:0] pout,
    output              nto1_pout
);


reg             din_vld;
reg [WIDTH-1:0] din_ff;

//avoid reset pulse
always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        din_vld <= 1'b0;
        din_ff[WIDTH-1:0] <= {(WIDTH){1'b0}};
    end 
    else begin  
        din_vld <= 1'b1;
        din_ff[WIDTH-1:0] <= din[WIDTH-1:0];
    end 
end

assign pout[WIDTH-1:0]  = ~din[WIDTH-1:0] & din_ff[WIDTH-1:0];
assign nto1_pout        = |pout[WIDTH-1:0];


endmodule


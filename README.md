# Atom （1000M Ethernet switch）

## 目录
### 1.介绍
### 2.硬件架构
### 3.Makefile
### 4.创建工程
<br>




#### 介绍
千兆以太网交换机，支持多种市场化功能，与商业交换机更接近，功能更丰富<br>
#### 硬件架构<br>
#### feature:
* 数据交换能力达到4000Mbps，即4 Port x 1000Mbps
* 支持2K MAC地址的存储与查询（包括MAC地址、Multicast table、VLAN table）
* 支持SM3或SHA-256的HASH算法
* 支持2MB的数据缓存区
* 支持基于端口的VLAN、基于MAC的VLAN
* 支持手动和动态触发的静态MAC VLAN配置
* 支持QinQ（802.1ad）、VLAN透传
* 支持多种VLAN映射方式
* 支持全双工10/100/1000 Mbps的GMII接口
* 发送端、接收端MAC支持AES数据流加解密
* 支持802.3x Flow Control
* 支持QoS，基于Port优先级或基于帧优先级进行仲裁输出
* 支持ACL，进行流量过滤、修改、防火墙功能等
* 支持Flush功能：全局Flush或者优先级Flush
* 支持风暴控制
* 支持流量统计

<br>

#### Makefile
makefile命令
1. make TESTNAME = {tb file name}

2. make verdi TESTNAME = {tb file name}<br>

使用makefile脚本时，每次编译都需要获知tb文件名，不便于使用.因此加入run脚本,输错命令时会自动输出tb文件名，优化了操作<br>
1.run -t {tb file name}   运行vcs编译文件<br>

2.run -v {tb file name}   运行verdi查看波形

<br>

#### 创建工程

1.clone project<br>

2.这里是列表文本在工程的同级建立一个软链接：ln -s ($10m-switch) project<br>

3.cd project<br>

4.source project/script/setup/atom.csh<br>

source后可以使用工程自带的快捷方式，如cdd进入design目录，cdv进入verify目录等

<br>

#### issue使用规则
1. 发现bug者、或者任务发起者发起issue，最好对应到具体milestone，所有信息需标明、指派人员、截至时间、优先级等；
2. 当发现指派给自己的任务后，如果开始解决问题了，需要将任务状态修改至 **进行中** ；
3. 被指派人员若不能及时完成、或发现架构层面更严重问题，需要评论原因；
4. 若完成任务或解决bug后，需要评论是何具体问题，如果解决等信息，便于之后遇到类似问题后的review；
5. 最后需要任务发布者确认是否完成，并修改任务状态为 **已完成** ；
